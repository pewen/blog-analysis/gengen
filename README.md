# gengen

## Setup

``` bash
pip install -r requeriments.txt
```

Intalar fasttext desde el
[repo](https://github.com/facebookresearch/fastText/#building-fasttext-for-python) (la versión de PyPI esta desactualizada) y jupyter-lab o jupyter-notebook para abrir los ipynb.

## Notebooks

[download_wiki_names](https://gitlab.com/pewen/blog-analysis/gengen/-/blob/master/download_wiki_names.ipynb): Baja nombres de personas de países hispanohablantes con su genero de wikidata usando SPARQL. La salida se entra en [data/nombres_con_genero.csv](https://gitlab.com/pewen/blog-analysis/gengen/-/raw/master/data/nombres_con_genero.csv)

[ train_model_fasttext.ipynb](https://gitlab.com/pewen/blog-analysis/gengen/-/blob/master/train_model_fasttext.ipynb): Modelo en [fasttext](https://github.com/facebookresearch/fastText) para predicción del género. Se usa en [gengen-cli](https://gitlab.com/pewen/blog-tools/gengen-cli/-/tree/master/)
